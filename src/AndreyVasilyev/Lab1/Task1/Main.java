
import java.util.HashMap;


public class Main{
    private static void collatz(long checkedNumber, long temp, HashMap<Long, Long> checkedNumbers) {
        checkedNumbers.replace(checkedNumber,checkedNumbers.get(checkedNumber), checkedNumbers.get(checkedNumber) + 1);
        if (temp == 1) return;
        else if (temp % 2 == 0) {
            if(checkTestedNumbers(checkedNumber, temp, checkedNumbers )){
                return;
            }
                else collatz(checkedNumber,temp / 2, checkedNumbers);
        }
        else {
            if(checkTestedNumbers(checkedNumber, temp, checkedNumbers )) {
                return;
            }
            else collatz(checkedNumber, 3 * temp + 1, checkedNumbers);
        }
    }
    private static boolean checkTestedNumbers (long checkedNumber, long temp, HashMap<Long, Long> checkedNumbers){
        if(checkedNumbers.containsKey(temp)){
            checkedNumbers.replace(checkedNumber,checkedNumbers.get(checkedNumber), checkedNumbers.get(checkedNumber) + checkedNumbers.get(temp));
            return false;
        }
        else return true;

    }



    public static void main(String[] args) {
        HashMap<Long, Long> checkedNumbers = new HashMap<>(1000000);
        long maxNumberOfIterations = 0;
        for (long checkedNumber = 1; checkedNumber != 1000000; checkedNumber++) {
            checkedNumbers.put(checkedNumber, (long)0);
            collatz(checkedNumber, checkedNumber, checkedNumbers);
            if(maxNumberOfIterations < checkedNumbers.get(checkedNumber)) {
                maxNumberOfIterations = checkedNumbers.get(checkedNumber);
            }
        }
        System.out.println("MaxNumberOfIterations: " + maxNumberOfIterations);
    }
}

