﻿

//Реализовать алгоритм вычисления квадратного корня. Взять за основу пример программы. Изменить пример так, чтобы была возможность регулирования точности расчетов.

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Sqrt
{
    double delta;
    double arg;

    Sqrt(double arg, double delta) {
        this.arg=arg;
        this.delta=delta;
    }
    double average(double x,double y) {
        return (x+y)/2.0;
    }
    boolean good(double guess,double x) {
        return Math.abs(guess*guess-x)<delta;
    }
    double improve(double guess,double x) {
        return average(guess,x/guess);
    }
    double iter(double guess, double x) {
        if(good(guess,x))
            return guess;
        else
            return iter(improve(guess,x),x);
    }


    public double calc() {
        return iter(1.0,arg);
    }
}

class Program
{
    public static void main(String[] args) throws IOException
    {
        while (true)
        {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Из какого числа нужно вычислить квадратный корень?");
            double val = Double.parseDouble(reader.readLine());
            System.out.println("Какая нужна точность? Число от 1 до 10:");
            double delta = (Double.parseDouble(reader.readLine()))/100000000;
            Sqrt sqrt=  new Sqrt(val, delta);
            double result = sqrt.calc();
            System.out.println("Квадратный корень из " + val + "=" + result);
        }
    }
}
