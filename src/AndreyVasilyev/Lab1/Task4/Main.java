import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException
    {
        int temp=0;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inputLine = reader.readLine();
        ArrayList<Integer> sequence = new ArrayList<>();
            for(char c: inputLine.toCharArray()){
            if(Character.isDigit(c)){
                temp=temp*10+Character.digit(c,10);
            }
            else if(c==','){
                sequence.add(temp);
                temp=0;
            }
        }
        sequence.add(temp);
        Enumeration.enumeration1(sequence);
    }
}

class Enumeration{
     public static int firstNum=0;
     public static int secondNum=0;
     public static boolean start=true;// старт итераций
     public static boolean check=true;//предыдущее число было меньше на единицу?


    public static void enumeration1(ArrayList sequence)
    {
        while (sequence.size()!=0)
        {
            if (start == true)
            {
                System.out.print(sequence.get(0));
                sequence.remove(0);
                start = false;
            } else if (sequence.size() == 1 && check == false)
            {
                System.out.print("-" + sequence.get(0));
                sequence.remove(0);
                break;
            } else if (sequence.size() == 1 && check == true)
            {
                System.out.print("," + sequence.get(0));
                sequence.remove(0);
                break;
            }
            firstNum = (int) sequence.get(0);
            secondNum = (int) sequence.get(1);
            if (secondNum - firstNum == 1 && check == true)
            {
                System.out.print("," + firstNum);
                sequence.remove(0);
                check = false;
                enumeration1(sequence);
            } else if (secondNum - firstNum == 1 && check == false)
            {
                sequence.remove(0);
                enumeration1(sequence);
            } else if (secondNum - firstNum > 1 && check == true)
            {
                System.out.print("," + firstNum);
                sequence.remove(0);
                check = true;
                enumeration1(sequence);
            } else if (secondNum - firstNum > 1 && check == false)
            {
                System.out.print("-" + firstNum);
                sequence.remove(0);
                check = true;
                enumeration1(sequence);
            }
        }
    }
}
