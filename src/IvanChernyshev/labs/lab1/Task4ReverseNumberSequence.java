

import java.util.*;

public class Task4ReverseNumberSequence {
	ArrayList<String> result;

	public static void main(String[] args) {
		new Task4ReverseNumberSequence().start();
	}

	public void start() {
		Scanner scanner = new Scanner(System.in);
		printHeader();
		String result = scanner.nextLine();
		printFirstStep();
		String firstStep = scanner.nextLine();
		printSecondStep();
		String secondStep = scanner.nextLine();
		scanner.close();
		parseUserInput(result, firstStep, secondStep);

	}

	private void parseUserInput(String result, String firstStep, String secondStep) {
		String[] results = result.split(",");
		ArrayList<String> res = new ArrayList<String>();
		Collections.addAll(res, results);
		deleteSequence(res, firstStep, secondStep);
		printResult();
	}

	private void deleteSequence(ArrayList<String> results, String firstStep, String secondStep) {
		int first = 0;
		int second = 0;
		for (String res : results) {
			if (res.equals(firstStep)) {
				first = results.indexOf(res);
			}
			if (res.equals(secondStep)) {
				second = results.indexOf(secondStep);
			}
		}
		String deleted = results.get(first) + "-" + results.get(second);
		while (second > first - 1) {
			results.remove(second);
			second--;
		}
		results.add(first, deleted);
		result = finalList(results);
	}

	private ArrayList<String> finalList(ArrayList<String> results) {
		ArrayList<String> finalList = new ArrayList<String>();
		for (String string : results) {
			string = string + ",";
			finalList.add(string);
		}
		return finalList;
	}

	private void printHeader() {
		System.out.println("������� ������������������ �����:");
	}

	private void printFirstStep() {
		System.out.println("�������� ������������������ �:");
	}

	private void printSecondStep() {
		System.out.println("��:");
	}

	private void printResult() {
		for (String res : result) {
			System.out.print(res + " ");
		}
	}
}
