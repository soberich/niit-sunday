

public class Task6Circle {
	private double radius;
	private double ference;
	private double area;

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
		findFerencePerRadius();
		findAreaPerRadius();
	}

	public double getFerence() {
		return ference;
	}

	public void setFerence(double ference) {
		this.ference = ference;
		findRadiusPerFerence();
		findAreaPerFerence();
	}

	public double getArea() {
		return area;
	}

	public void setArea(double area) {
		this.area = area;
		findRadiusPerArea();
		findFerencePerArea();
	}

	private void findFerencePerRadius() {
		this.ference = 2 * Math.PI * radius;
	}

	private void findAreaPerRadius() {
		this.area = Math.PI * Math.pow(radius, 2);
	}

	private void findRadiusPerFerence() {
		this.radius = ference / (2 * Math.PI);
	}

	private void findRadiusPerArea() {
		this.radius = Math.sqrt(area / Math.PI);
	}

	private void findFerencePerArea() {
		this.ference = 2 * (Math.sqrt(Math.PI * area));
	}

	private void findAreaPerFerence() {
		this.area = Math.pow(ference, 2) / (4 * Math.PI);
	}

	public void printFields() {
		System.out.println("�������������� ����������:");
		System.out.println("������: " + String.format("%.2f", radius));
		System.out.println("����� ����������: " + String.format("%.2f", ference));
		System.out.println("�������: " + String.format("%.2f", area));
	}

}
