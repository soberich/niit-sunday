
public class CoffeMachineDemo {
	public static void main(String[] args) {
		CoffeMachine coffe = new CoffeMachine();
		// firstScenario(coffe);
		secondScenario(coffe);
	}

	static void firstScenario(CoffeMachine coffe) {
		startWorking(coffe);
		putMoney(coffe);
		choiseDrink(coffe);
		putMoney(coffe);
		choiseDrink(coffe);
		endWorking(coffe);
	}

	static void secondScenario(CoffeMachine coffe) {
		putMoney(coffe);

		startWorking(coffe);
		choiseDrink(coffe);
		putMoney(coffe);
		cancel(coffe);
		
		putMoney(coffe);
		choiseDrink(coffe);
		endWorking(coffe);
		
		choiseDrink(coffe);
		
		startWorking(coffe);
		putMoney(coffe);
		choiseDrink(coffe);
		putMoney(coffe);
		choiseDrink(coffe);
		endWorking(coffe);
	}

	private static void printDrinkWithPrise(CoffeMachine coffe) {
		for (int i = 0; i < coffe.getMenu().length; i++) {
			System.out.println(coffe.getMenu()[i] + " " + coffe.getPrices()[i]);
		}
	}

	static void printResume(String resume) {
		System.out.println(resume);
	}

	static void printState(CoffeMachine coffe) {
		System.out.println("������� ��������� �������� " + coffe.getState() + "\n");
	}

	static void startWorking(CoffeMachine coffe) {
		coffe.on();
		printState(coffe);
		printDrinkWithPrise(coffe);
	}

	static void endWorking(CoffeMachine coffe) {
		coffe.off();
		printState(coffe);
	}

	static void putMoney(CoffeMachine coffe) {
		coffe.coin(15);
		printState(coffe);
	}

	static void choiseDrink(CoffeMachine coffe) {
		coffe.choise(0);
		printState(coffe);
	}

	static void cancel(CoffeMachine coffe) {
		coffe.cancel();
		printState(coffe);
	}
}
