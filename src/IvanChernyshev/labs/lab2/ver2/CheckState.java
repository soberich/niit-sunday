
public class CheckState extends States {

	public CheckState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ��� �������";
		printResume();
	}

	@Override
	public void off() {
		resume = "������ ��� ��������� �������, ����� ��������� ��������";
		printResume();
	}

	@Override
	public void coin(int cash) {
		resume = "���� ��� ��� ��������";
		printResume();
	}

	@Override
	public void choise() {
		resume = "������� ��� ������";
		printResume();
	}

	@Override
	public void check() {
		if (coffe.getCash() == coffe.getPrices()[coffe.getIndexOfDrink()]) {
			resume = "�������� ������ �������";
			printResume();
			coffe.setCash(coffe.getCash() - coffe.getPrices()[coffe.getIndexOfDrink()]);
			coffe.setCurrentState(coffe.getCookState());

		} else if (coffe.getCash() > coffe.getPrices()[coffe.getIndexOfDrink()]) {
			coffe.setCash(coffe.getCash() - coffe.getPrices()[coffe.getIndexOfDrink()]);
			resume = "�������� ������ �������, ��� ������� " + coffe.getCash();
			printResume();
			coffe.setCurrentState(coffe.getCookState());
		} else if (coffe.getCash() < coffe.getPrices()[coffe.getIndexOfDrink()]) {
			resume = "���������� ������ " + (coffe.getPrices()[coffe.getIndexOfDrink()] - coffe.getCash() + " ������");
			printResume();
			coffe.setCurrentState(coffe.getWaitState());
		}
	}

	@Override
	public void cancel() {
		resume = "�������� ������� " + coffe.getCash() + " ������";
		coffe.setCash(0);
		printResume();
		coffe.setCurrentState(coffe.getWaitState());
	}

	@Override
	public void cook() {
		resume = "� ������ ������ �������� �� ��������, ���� ��������";
		printResume();
	}

	@Override
	public void finish() {
		resume = "������� ����� ����������� �������";
		printResume();
	}
}
