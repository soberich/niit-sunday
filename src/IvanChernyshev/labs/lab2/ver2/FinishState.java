
public class FinishState extends States {

	public FinishState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ��� �������";
		printResume();
	}

	@Override
	public void off() {
		resume = "������ ��� ��������� �������, ����� ��������� ��������";
		printResume();
	}

	@Override
	public void coin(int cash) {
		resume = "���� ��� ��� ��������";
		printResume();
	}

	@Override
	public void choise() {
		resume = "������� ��� ������";
		printResume();
	}

	@Override
	public void check() {
		resume = "������ ��� ��������";
		printResume();
	}

	@Override
	public void cancel() {
		resume = "��������� � ������ ������ ������, ����������� ������ ��������";
		printResume();
	}

	@Override
	public void cook() {
		resume = "������� ��� ��� �����������";
		printResume();
	}

	@Override
	public void finish() {
		if (coffe.getCash() > 0) {
			resume = "�������� ������� " + coffe.getCash() + " ������";
			printResume();
			coffe.setCash(0);
		}
		resume = "�������� ��� ���������� ��� �������!!!";
		printResume();
		coffe.setCurrentState(coffe.getWaitState());
	}
}
