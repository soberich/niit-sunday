
public class WaitState extends States {

	public WaitState(CoffeMachine coffe) {
		super(coffe);
	}

	@Override
	public void on() {
		resume = "������� ��� �������";
		printResume();
	}

	@Override
	public void off() {
		if (coffe.getCash() > 0) {
			resume = "�������� ������� " + coffe.getCash() + " ������";
			printResume();
			coffe.setCash(0);
		}
		resume = "������� ������� ��������";
		printResume();
		coffe.setCurrentState(coffe.getOffState());
	}

	@Override
	public void coin(int cash) {
		coffe.setCash(coffe.getCash() + cash);
		resume = "����� ����� �� ����";
		printResume();
		coffe.setCurrentState(coffe.getAcceptState());
	}

	@Override
	public void choise() {
		resume = "������ ��� ������� �������, ��������� ����";
		printResume();
	}

	@Override
	public void check() {
		resume = "������ ��� ������� �������, ��������� ����";
		printResume();
	}

	@Override
	public void cancel() {
		resume = "������ ��� ���������, ������ ����� ������";
		printResume();
	}

	@Override
	public void cook() {
		resume = "������ ��� ����������� �������, ��������� ����";
		printResume();

	}

	@Override
	public void finish() {
		resume = "������ ��� ���������, ������ ����� ������";
		printResume();
	}

}
