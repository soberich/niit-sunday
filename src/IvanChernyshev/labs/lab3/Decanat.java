
import java.io.*;
import java.util.*;

public class Decanat {
	private ArrayList<Student> students;
	private ArrayList<Group> groups;

	public Decanat() {
		this.students = new ArrayList<Student>();
		this.groups = new ArrayList<Group>();
		createGroups();
		createStudents();
		divisionStudents();
	}

	public ArrayList<Student> getStudents() {
		return students;
	}

	public ArrayList<Group> getGroups() {
		return groups;
	}

	private void createGroups() {
		String[] arrayGroups = getDescriptionFromFile("groups.txt").split(",");
		for (int i = 0; i < arrayGroups.length; i++) {
			Group group = new Group(arrayGroups[i]);
			groups.add(group);
		}
	}

	private void createStudents() {
		int indexID = 1;
		String[] arrayNames = getDescriptionFromFile("names.txt").split(",");
		for (int i = 0; i < arrayNames.length; i++) {
			Student student = new Student(indexID, arrayNames[i]);
			indexID++;
			students.add(student);
		}
	}

	private String getDescriptionFromFile(String string) {
		StringBuilder stringsFromFile = new StringBuilder();
		BufferedReader reader = new BufferedReader(new InputStreamReader(Decanat.class.getResourceAsStream(string)));
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				stringsFromFile = stringsFromFile.append(line + ",");
			}
		} catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("�� ������� ��������� ������ �� �����");
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("�� ������� ������� BufferedReader");
			}
		}
		return stringsFromFile.toString();
	}

	private void divisionStudents() {
		ArrayList<Student> localStudents = new ArrayList<Student>(students);
		for (int i = 0; i < groups.size(); i++) {
			if (groups.size() > 0) {
				for (int a = 0; a < (students.size() / groups.size()); a++) {
					int index = RandomGenerator.getRandom(0, localStudents.size());
					if (index == localStudents.size()) {
						index--;
					}
					Student student = localStudents.get(index);
					student.setGroup(groups.get(i));
					groups.get(i).addStudent(student);
					localStudents.remove(student);
				}
			}
			groups.get(i).setHead(groups.get(i).getStudents()
					.get(RandomGenerator.getRandom(0, groups.get(i).getStudents().size() - 1)));
			groups.get(i).sortCollection();
		}
	}

	public void addMarks() {
		for (Group group : groups) {
			for (Student student : group.getStudents()) {
				student.addMark(RandomGenerator.getRandom(0, 5));
			}
		}
	}

	public void relocationStudents(Group one, Group two) {
		int randomStudent = RandomGenerator.getRandom(0, one.getStudents().size() - 1);
		two.addStudent(one.getStudents().get(randomStudent));
		one.removeStudent(randomStudent);
		one.setHead(one.getStudents().get(RandomGenerator.getRandom(0, one.getStudents().size() - 1)));
	}

	public void dismissStudents() {
		for (Student student : students) {
			if (student.averageMarkCalc() < 3.0) {
				student.getGroup().removeStudent(student);
				if (student == student.getGroup().getHead()) {
					student.getGroup().setHead(null);
				}
			}
		}
		for (Group group : groups) {
			try {
				if (group.getHead() == null | group.getStudents().size() != 0) {
					if (group.getStudents().size() == 1) {
						group.setHead(group.getStudents().get(0));
					} else if (group.getStudents().size() > 1) {
						group.setHead(
								group.getStudents().get(RandomGenerator.getRandom(0, group.getStudents().size() - 1)));
					}
				}
			} catch (IndexOutOfBoundsException ex) {
				ex.printStackTrace();
				System.out.println(group.getStudents().size());
			}
		}
	}

	public void printFields() {
		for (Group group : groups) {
			group.printInfo();
		}
		// for (Student student : students) {
		// System.out.println(student + " " + student.getNum() + " " +
		// student.averageMarkCalc()+" "+student.getMarks());
		// }
	}

	public void printFullFields() {
		for (Group group : groups) {
			group.printFullInfo();
		}
	}

}
