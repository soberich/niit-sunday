
import java.io.*;

public class DecanatDemo {
	static final String begin = "result1.txt";
	static final String end = "result2.txt";

	public static void main(String[] args) {
		Decanat decanat = new Decanat();
		System.out.println("��������� ������ �� ���� �������" + "\n");
		decanat.printFields();
		saveData(decanat, begin);
		System.out.println("������� ��������� �� ������ � ������" + "\n");
		for (int i = 0; i < decanat.getGroups().get(0).getStudents().size() - 4; i++) {
			decanat.relocationStudents(decanat.getGroups().get(0), decanat.getGroups().get(2));
		}
		decanat.printFields();
		System.out.println("������� ��������� �� ������ � ������" + "\n");
		for (int i = 0; i < 5; i++) {
			System.out.println("���������� ������, ���������� ���������, �����" + "\n");
			firstScenario(decanat);
		}
		decanat.printFullFields();
		saveData(decanat, end);
	}

	static void firstScenario(Decanat decanat) {
		for (int i = 0; i < 10; i++) {
			decanat.addMarks();
		}
		decanat.dismissStudents();
		decanat.printFields();
	}

	static void saveData(Decanat decanat, String file) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new FileWriter(new File(file)));
			for (Group group : decanat.getGroups()) {
				writer.append("������ " + group.getTitle());
				writer.newLine();
				writer.append("���������� �������� " + group.getNum());
				writer.newLine();
				if (group.averageMarkCalc() > 0) {
					writer.append("������� ������ ������ " + String.format("%.2f", group.averageMarkCalc()));
					writer.newLine();
				}
				if (group.getHead() != null) {
					writer.append("�������� ������ " + group.getHead());
					writer.newLine();
				}
				if (group.getNum() > 0) {
					writer.append("������:");
					for (Student student : group.getStudents()) {
						writer.newLine();
						writer.append("��� ��������: " + student.getFio());
						writer.newLine();
						writer.append("ID �����: " + student.getID());
						writer.newLine();
						if (student.getMarks().size() > 0) {
							writer.append("������: " + student.getMarks());
							writer.newLine();
						}
						if (student.averageMarkCalc() > 0) {
							writer.append("������� ������ " + student.averageMarkCalc());
							writer.newLine();
						}
					}
				} else {
					writer.append("� ������ �������� �� ��������");
					writer.newLine();
				}
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
