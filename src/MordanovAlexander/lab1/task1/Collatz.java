public class Collatz {
    public static void main(String[] args) {
        final int min_i = 1; // начальное значение
        final int max_i = 1000000; //длина последовательности
        CollatzConjecture cltz = new CollatzConjecture(min_i, max_i);

        System.out.println("Maximum conjurity length for [" + min_i + ".." + max_i + "] = " + cltz.GetMaxLength() + " (number: " + cltz.GetMaxValue() + ")");
    }

}

class CollatzConjecture { // класс для расчета последовательности
    int init_number_min=1; //какой последовательностью инициалиируем (минимум)
    int init_number_max=100; //какой последовательностью инициалиируем (максимум)
    int max_counter=0; //указатель максимальной длины последовательности
    long max_value=1; //число, генерирующее самую длинную последовательность

    public CollatzConjecture(int num_min, int num_max) { //конструктор
        this.init_number_min = num_min;
        this.init_number_max = num_max;
        System.out.println("Conjecture initialization... Init range = [" + init_number_min + ".." + init_number_max + "]");

        int i;
        int len;
        for(i=init_number_min;i<=init_number_max;i++) {
            len=GetLengthForNumber(i); // найдем длину последовательности для конкретного значения
            //System.out.println("Length for " + i + " = " + len);
            if(len > max_counter) {
                max_counter = len; // если значение максимальное - запомним
                max_value = i;
            }
        }
    }

    private int GetLengthForNumber(long num) {
        int len=0;
        do {
            if((num & 1) == 0) {num = num >> 1;} else {num=3*num+1;} // следующее число в последовательности
            len++; // счетчик длины последовательности
        } while (num!=1); // считаем пока не найдем признак конца подсчета
        return len;
    }

    public int GetMaxLength() {
        return max_counter;
    }
    public long GetMaxValue() { return max_value; }
}
