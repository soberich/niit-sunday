public class expander {
    public static void main(String[] args) {
        if(args.length < 1) { //проверяем что есть параметр
            System.out.println("Parameters: [1] - string for expand");
            System.out.println("String format: [N],[N-M],...");
            return;
        }
        Expand exp = new Expand(args[0]);
        System.out.println("Result = " + exp.expandAll());
    }
}

class Expand {
    String sourcestr; // исходная строка
    String[] words; // массив разбитых слов

    Expand (String srcstr) {
        this.sourcestr = srcstr;
        this.words = sourcestr.split(","); //делим на слова через зпт
    }

    public String expandAll() {
        String res = "";
        for(int i=0;i<words.length;i++) { // развернем каждое слова
            res = res + expand(words[i]);
        }
        if(res.length()>1) {
            res = res.substring(0, res.length() - 1); // убрать лишнюю запятую в конце,
        }
        return res;
    }

    private String expand(String exp) {
        String texp1, texp2;
        int p=-1;

        p=exp.indexOf("-");
        switch(p) {
            case -1: // для обычных натуральных чисел
                if(isNumber(exp)) return exp+",";
                break;
            case 0: // для отрицательных чисел
                texp1=exp.substring(1);
                if(isNumber(texp1)) return "-"+texp1+",";
                break;
            default:
                String lstr="";

                texp1=exp.substring(0,p);               // первое число
                texp2=exp.substring(p+1,exp.length());  // второе число
                if((isNumber(texp1))&&(isNumber(texp2))) {
                    for(int k=Integer.parseInt(texp1);k<=Integer.parseInt(texp2);k++) // циклик для вывода чисел
                        lstr=lstr+k+",";
                    return lstr;
                }
                else
                    return "";
        }
        return "NaN"; // если не прошли ни одной проверки - значит дали не число
    }

    private boolean isNumber(String str) { // функция проверки на число
        if (str == null || str.isEmpty()) return false;
        for (int i = 0; i < str.length(); i++) {
            if (!Character.isDigit(str.charAt(i))) return false;
        }
        return true;
    }
}