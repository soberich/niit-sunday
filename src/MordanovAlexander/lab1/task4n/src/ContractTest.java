import org.junit.Test;

import static org.junit.Assert.*;

public class ContractTest {
    @Test
    public void contractAll1() throws Exception {
        Contract ctr = new Contract("1,2,3,4,5,6,7,8");
        assertTrue(ctr.contractAll().equals("1-8"));
    }

    @Test
    public void contractAll2() throws Exception {
        Contract ctr = new Contract("1,3,5,7,9,11,13,15");
        assertTrue(ctr.contractAll().equals("1,3,5,7,9,11,13,15"));
    }

    @Test
    public void contractAll3() throws Exception {
        Contract ctr = new Contract("1,2,3,5,7,8,9");
        assertTrue(ctr.contractAll().equals("1-3,5,7-9"));
    }

    @Test
    public void contractAll4() throws Exception {
        Contract ctr = new Contract("1,3,4,5,7,9,10,11,13");
        assertTrue(ctr.contractAll().equals("1,3-5,7,9-11,13"));
    }

    @Test
    public void contractAll5() throws Exception {
        Contract ctr = new Contract("1,2,4,5,7,8,10,11,12");
        assertTrue(ctr.contractAll().equals("1,2,4,5,7,8,10-12"));
    }

}