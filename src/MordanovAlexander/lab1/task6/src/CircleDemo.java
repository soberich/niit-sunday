import java.text.DecimalFormat;
import java.text.NumberFormat;

import static java.lang.Math.*;

public class CircleDemo {

    public static void main(String[] args) {
	    resolveTask1();
	    resolveTask2();
    }

    public static void resolveTask1() { // земля и веревка
        final double EarthRadius = 67381000; // радиус планеты Земля (в метрах)
        Circle crcl = new Circle(EarthRadius, 0);
        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Task 1 - Earth and rope");
        System.out.println("Radius will change to " + formatter.format(crcl.setFerence(crcl.getFerence() + 1).getRadius() - EarthRadius) + " meters");
        System.out.println();
    }

    public static void resolveTask2() { // бассейн
        final double PoolRadius = 3; // радиус бассейна
        final double ConcreteWidth = 1; // радиус бетонной дорожки
        final long ConcreteAreaPrice = 1000; // стоимость кв. метра бетонного покрытия
        final long FenceLinePrice = 2000; // стоимость погонного метра ограды

        Circle Pool = new Circle(PoolRadius, 0);  // создаем объект круга для бассейна
        Circle Fence = new Circle(PoolRadius + ConcreteWidth, 0); // создаем объект круга для бассейна с дорожкой, опоясанной оградой

        NumberFormat formatter = new DecimalFormat("#0.00");
        System.out.println("Task 2 - Pool");
        double FenceSum = Fence.getFerence() * FenceLinePrice; // стоимость ограды = длина ограды * цену за п/м
        double ConcreteSum = (Fence.getArea() - Pool.getArea()) * ConcreteAreaPrice; // площадь дорожки = площадь бассейна с дорожкой - площадь бассейна
        System.out.println("Fence price: " + formatter.format(FenceSum));
        System.out.println("Concrete price: " + formatter.format(ConcreteSum));
        System.out.println("--------------------");
        System.out.println("Total: " + formatter.format(FenceSum + ConcreteSum));
    }
}

class Circle {
    private double Radius;
    private double Ference;
    private double Area;

    public Circle(double Value, int ValueType) {
        switch(ValueType) {
            case 1:                  // если передали 1, то значит это длина окружности
                setFerence(Value);
                break;
            case 2:                  // если передали 2, то значит передали площадь круга
                setArea(Value);
                break;
            default:
                setRadius(Value);    // все иное - передали радиус
        }
    }

    public Circle setRadius(double Radius) {
        this.Radius = Radius;
        this.Ference = 2*Math.PI*Radius;
        this.Area = Math.PI*Math.pow(Radius, 2);
        return this;
    }

    public Circle setFerence(double Ference) {
        this.Ference = Ference;
        this.Radius = Ference / (2 * Math.PI);
        this.Area = this.Radius * (Ference / 2);
        return this;
    }

    public Circle setArea(double Area) {
        this.Area = Area;
        this.Radius = Math.sqrt(Area/Math.PI);
        this.Ference = 2*Math.PI*this.Radius;
        return this;
    }

    public double getRadius() {
        return Radius;
    }

    public double getFerence() {
        return Ference;
    }

    public double getArea() {
        return Area;
    }

}