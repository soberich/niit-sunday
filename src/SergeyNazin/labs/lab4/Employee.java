
/**
 * Employee
 */
public abstract class Employee {

    public int id;// - идентификационный номер.
    public String name;// - ФИО.
    //public int base;
    //public String project;

    //public String position;
    public int worktime;// - отработанное время.
    public int payment;// - заработная плата.

    public Employee (int id, String name) {
        this.id = id;
        this.name = name;
        //this.worktime = worktime;
        //this.payment = payment;
        //this.base = base;
        //this.project = project;
    }
    
    public abstract void getPaid();
}