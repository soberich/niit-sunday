
/**
 * Personal
 */
public abstract class Personal extends Employee implements WorkTime {
    public final int base;
    public Personal (int id, String name, int base) {
        super(id, name);
        this.base = base;
    }
    @Override
    public void getPaid() {
        multiplyHoursToBaseWageRate(this.base, this.worktime);
    }
    @Override
    public void multiplyHoursToBaseWageRate(int base, int hours) {
        this.payment += base * hours;
    }
}