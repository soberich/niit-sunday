
/**
 * Programmer
 */
public class Programmer extends Engineer {

    public Programmer (int id, String name, int base, int worktime, int budget, int amountOfColleaguesInvolved) {
        super(id, name, base, worktime, budget, amountOfColleaguesInvolved);
    }
}