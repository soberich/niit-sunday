

/**
 * ProjectManager
 */
public class ProjectManager extends Manager implements Heading {
    public final int numSubordinates;
    public ProjectManager (int id, String name, int budget, int amountOfColleaguesInvolved, int numSubordinates) {
        super(id, name, budget, amountOfColleaguesInvolved);
        this.numSubordinates = numSubordinates;
    }
    @Override
    public void getPaid() {
        super.getPaid();
        getPaidForSubordinates(this.numSubordinates);
    }
    @Override
    public void getPaidForSubordinates(int numSubordinates) {
        payment += numSubordinates * Heading.CASH_FOR_SUBORDINATE;
    }
}