

/**
 * SeniorManager
 */
public class SeniorManager extends ProjectManager {

    public SeniorManager (int id, String name, int budget, int amountOfColleaguesInvolved, int numSubordinates) {
        super(id, name, budget, amountOfColleaguesInvolved, numSubordinates);
    }
}