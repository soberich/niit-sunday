

/**
 * TeamLeader
 */
public class TeamLeader extends Programmer implements Heading {
    public final int numSubordinates;
    public TeamLeader (int id, String name, int base, int worktime, int budget, int amountOfColleaguesInvolved, int numSubordinates) {
        super(id, name, base, worktime, budget, amountOfColleaguesInvolved);
        this.numSubordinates = numSubordinates;
    }
    @Override
    public void getPaid() {
        super.getPaid();
        getPaidForSubordinates(this.numSubordinates);
    }
    @Override
    public void getPaidForSubordinates(int numSubordinates) {
        payment += numSubordinates * Heading.CASH_FOR_SUBORDINATE;
    }
}