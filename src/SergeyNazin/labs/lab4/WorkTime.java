


/**
 * WorkTime
 */
public interface WorkTime {
    void multiplyHoursToBaseWageRate(int base, int hours);
}