//package lab_1;

/**
 * Лабораторная №1, задание №1 Последовательность Коллатца, вариант с рекурсией
 */


public class lab_1_1_recursion {


    public static void main(String[] args) {

        long maxLenth = 0;
        long numOfMaxLenth = 0;
        for (int i = 1; i < 1_000_001; i++) {
            if (CollatzLenth(i, 0) > maxLenth) {
                maxLenth = CollatzLenth(i, 0);
                numOfMaxLenth = i;
            }
        }
        System.out.println("Максимальная последовательность Коллатца на отрезке от 1 до 1000000 составляет: " + maxLenth + " для числа " + numOfMaxLenth);
    }


    /*Метод CollatzLenth принимает число и возвращает длину последовательности Коллатца для этого числа */
    static long CollatzLenth(long numb, long lenth) {
        //lenth++;
        if (numb == 1) return lenth + 1;
        else if (numb % 2 == 0) return CollatzLenth(numb / 2, lenth + 1);
        else return CollatzLenth(numb * 3 + 1, lenth + 1);
        //return lenth++;
    }

}
