package lab3;

/**
 * Лабораторная №3
 * <p>
 * Данные о студентах будут храниться в файле в виде строки
 * по следующему образцу:  "170000_1_0_Иванов И.И._4,4,4,4,4" .
 * Используя метод split("_") создам из строки массив строк, в котором:
 * [0]-элемент это индекс студента
 * [1]-элемент это идентификационный номер группы
 * [2]-элемент это указатель, является ли студент старостой(0-нет, 1-да)
 * [3]-элемент это фамилия.имя.отчество
 * [4]-элемент это массив оценок
 * Количество оценок ограничено пятью(итоговые по каждому предмету)
 * Количество меньше пяти означает незакрытый предмет
 */
public class Student {


    private final int ID;
    private final String FIO;
    int groupID;
    Group groupReference;
    String Marks = "";
    boolean head = false;

    //Конструктор
    public Student(String FIO, int ID) {
        if (FIO.length()<2 && Integer.toString(ID).length() < 2) {
            throw new IllegalArgumentException();
        }
        this.FIO = FIO;
        this.ID = ID;
    }

    public int getID() {
        return ID;
    }

    public String getFIO() {
        return FIO;
    }


    //Зачисление студента в группу
    public void addToGroup(Group group) {
        this.groupID = group.getGroupID();
        this.groupReference = group;
        group.studentsReferences.add(this);
    }

    //Добавление оценки
    public void addMark(int mark) {
        if (numberOfMarks() < 5) {
            if (mark == 3 || mark == 4 || mark == 5)
                if (Marks.length() == 0) Marks += Integer.toString(mark);
                else Marks += ("," + Integer.toString(mark));
            else System.out.println("Введена некорректная оценка!");
        } else System.out.println("Студент уже получил все оценки");

    }

    //Количество оценок
    public int numberOfMarks() {
        String[] numOfMarks = Marks.split(",");
        return numOfMarks.length;
    }

    //Вычисление средней оценки
    public double calcAverageMark() {
        double elementsSum = 0;
        if(numberOfMarks()<2){
            System.out.println("недостаточно данных для рассчета");
            return 0;
        }
        else {
            String[] marksToStringArray = Marks.split(",");
            int[] marksToIntArray = new int[marksToStringArray.length];
            for (int i = 0; i < marksToIntArray.length; i++) {
                marksToIntArray[i] = Integer.parseInt(marksToStringArray[i]);
                elementsSum += marksToIntArray[i];
            }
            return elementsSum / marksToIntArray.length;
        }

    }


    @Override
    public String toString() {
        if (head) {
            return FIO + " -> группа: " + "<< " + groupReference.getGroupNAME() + " >>" + ", является старостой " + ", средний балл: " + String.format("%.1f",calcAverageMark())+ ", оценки: " + Marks;
        } else if(groupReference != null){
            return  FIO + " -> группа: " +"<< "+ groupReference.getGroupNAME() +" >>"+ ", средний балл: " + String.format("%.1f",calcAverageMark())+ "; оценки: " + Marks;
        }else {
            return FIO + " -> " + " в процессе перевода в другую группу" + ", средний балл: " + String.format("%.1f",calcAverageMark())+ "; оценки: " + Marks;
        }
    }

}
