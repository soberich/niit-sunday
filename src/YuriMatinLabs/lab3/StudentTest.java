package lab3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 *   Тест
 */
public class StudentTest {
    @Test
    public void calcAverageMark() throws Exception {
        Student student = new Student("Вася",123456);
        student.Marks = "4,4,5,5";
        double di = student.calcAverageMark();
        assertEquals(4.5, di, 0.1);
    }

}