/**
 * Родительский класс "Работник"
 */
public abstract class Employee {
    String position;
    private final int ID;
    private final String name;
    int workTme;
    double payment;


    public Employee(int ID, String name) {
        this.ID = ID;
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public double getPayment() {
        return payment;
    }

    abstract void calcPayment();
}
