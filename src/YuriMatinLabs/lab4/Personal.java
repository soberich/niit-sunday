/**
 * Персонал, родительский класс для уборщицы и водителя
 */
public abstract class Personal extends Employee implements WorkTime {
    //поле "базовая ставка"
    int BASE;

    public Personal(int ID, String name, int workTme, int BASE) {
        super(ID, name);
        this.workTme=workTme;
        this.BASE=BASE;
    }

    void calcPayment(){
        this.payment = getPaymentForWorkTime();
    }

    @Override
    public int getPaymentForWorkTime() {
         return BASE * workTme;
    }

    @Override
    public String toString() {
        return "Сотрудник "+getName()+" ID: "+getID()+"; занимаемая должность: "
                +position+"; отработанное время: "+Integer.toString(workTme)
                +" часов"+"; начисленная зарплата: "+Double.toString(getPayment())+" рублей" ;
    }
}
