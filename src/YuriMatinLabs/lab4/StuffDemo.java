import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Лабораторная №4
 */
//Класс текущий проект
class CurrentProject {
    String name;
    //ArrayList<Employee> members = new ArrayList<>();
    private int budget;
    int numOfMembers;


    public CurrentProject(String name, int budget) {
        this.name = name;
        this.budget = budget;
    }

    public int getBudget() {
        return budget;
    }
}

public class StuffDemo {
    public static void main(String[] args) {
        //Список всех сотрудников
        ArrayList<Employee> allMembers = new ArrayList<>();

        CurrentProject undWaterDrone = new CurrentProject("Underwater Drone", 100_000);
        CurrentProject mobileGame = new CurrentProject("Mobile Game", 40_000);
        //Извлекаю данные из json-файла
        //Извиняюсь за дублирование кода; пока не смог придумать, как написать некий универсальный метод
        try {
            JSONParser parser = new JSONParser();
            FileReader emplReader = new FileReader("Employers.json");
            Object object = parser.parse(emplReader);
            JSONObject jsonObject = (JSONObject) object;
            //Создаю и добавляю водителей
            JSONArray driversArray = (JSONArray) jsonObject.get("driver");
            for (Object i : driversArray) {
                Driver driver = new Driver(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        ((Long) ((JSONObject) i).get("worktime")).intValue(),
                        ((Long) ((JSONObject) i).get("base")).intValue());
                driver.calcPayment();
                allMembers.add(driver);
            }
            //Создаю и добавляю уборщиц
            JSONArray cleanerArray = (JSONArray) jsonObject.get("cleaner");
            for (Object i : cleanerArray) {
                Cleaner cleaner = new Cleaner(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        ((Long) ((JSONObject) i).get("worktime")).intValue(),
                        ((Long) ((JSONObject) i).get("base")).intValue());
                cleaner.calcPayment();
                allMembers.add(cleaner);
            }
            //Создаю и добавляю программистов
            JSONArray programmersArray = (JSONArray) jsonObject.get("Programmer");
            for (Object i : programmersArray) {
                int currentBudget = 0;
                //Проверяю, в каком проекте учавствует работник
                if (((JSONObject) i).get("project").equals("Underwater Drone")) {
                    undWaterDrone.numOfMembers++;
                    currentBudget = undWaterDrone.getBudget();
                } else if (((JSONObject) i).get("project").equals("Mobile Game")) {
                    mobileGame.numOfMembers++;
                    currentBudget = mobileGame.getBudget();
                }
                Programmer programmer = new Programmer(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        ((Long) ((JSONObject) i).get("worktime")).intValue(),
                        ((Long) ((JSONObject) i).get("base")).intValue(),
                        (((JSONObject) i).get("project")).toString(),
                        currentBudget,
                        ((Long) ((JSONObject) i).get("partInProject")).doubleValue());
                programmer.calcPayment();
                allMembers.add(programmer);
            }
            //Создаю и добавляю тестеров
            JSONArray testersArray = (JSONArray) jsonObject.get("Tester");
            for (Object i : testersArray) {
                int currentBudget = 0;
                //Проверяю, в каком проекте учавствует работник
                if (((JSONObject) i).get("project").equals("Underwater Drone")) {
                    undWaterDrone.numOfMembers++;
                    currentBudget = undWaterDrone.getBudget();
                } else if (((JSONObject) i).get("project").equals("Mobile Game")) {
                    mobileGame.numOfMembers++;
                    currentBudget = mobileGame.getBudget();
                }
                Tester tester = new Tester(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        ((Long) ((JSONObject) i).get("worktime")).intValue(),
                        ((Long) ((JSONObject) i).get("base")).intValue(),
                        (((JSONObject) i).get("project")).toString(),
                        currentBudget,
                        ((Long) ((JSONObject) i).get("partInProject")).doubleValue());
                tester.calcPayment();
                allMembers.add(tester);
            }
            //Создаю и добавляю тимлидов
            JSONArray teamLeadersArray = (JSONArray) jsonObject.get("TeamLeader");
            for (Object i : teamLeadersArray) {
                int currentBudget = 0;
                int numOfSub = 0;
                //Проверяю, в каком проекте учавствует работник
                if (((JSONObject) i).get("project").equals("Underwater Drone")) {
                    currentBudget = undWaterDrone.getBudget();
                    numOfSub = undWaterDrone.numOfMembers;
                    undWaterDrone.numOfMembers++;
                } else if (((JSONObject) i).get("project").equals("Mobile Game")) {
                    currentBudget = mobileGame.getBudget();
                    numOfSub = undWaterDrone.numOfMembers;
                    mobileGame.numOfMembers++;
                }
                TeamLeader teamLeader = new TeamLeader(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        ((Long) ((JSONObject) i).get("worktime")).intValue(),
                        ((Long) ((JSONObject) i).get("base")).intValue(),
                        (((JSONObject) i).get("project")).toString(),
                        currentBudget,
                        ((Long) ((JSONObject) i).get("partInProject")).doubleValue(),
                        numOfSub);
                teamLeader.calcPayment();
                allMembers.add(teamLeader);
            }
            //Создаю и добавляю менеджеров
            JSONArray managersArray = (JSONArray) jsonObject.get("Manager");
            for (Object i : managersArray) {
                int currentBudget = 0;
                //Проверяю, в каком проекте учавствует работник
                if (((JSONObject) i).get("project").equals("Underwater Drone")) {
                    undWaterDrone.numOfMembers++;
                    currentBudget = undWaterDrone.getBudget();
                } else if (((JSONObject) i).get("project").equals("Mobile Game")) {
                    mobileGame.numOfMembers++;
                    currentBudget = mobileGame.getBudget();
                }
                Manager manager = new Manager(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        (((JSONObject) i).get("project")).toString(),
                        currentBudget,
                        ((Long) ((JSONObject) i).get("partInProject")).doubleValue());
                manager.calcPayment();
                allMembers.add(manager);
            }
            //Создаю и добавляю проект-менеджеров
            JSONArray projManagersArray = (JSONArray) jsonObject.get("ProjectManager");
            for (Object i : projManagersArray) {
                int currentBudget = 0;
                int numOfSub = 0;
                //Проверяю, в каком проекте учавствует работник
                if (((JSONObject) i).get("project").equals("Underwater Drone")) {
                    numOfSub = undWaterDrone.numOfMembers;
                    currentBudget = undWaterDrone.getBudget();
                    undWaterDrone.numOfMembers++;
                } else if (((JSONObject) i).get("project").equals("Mobile Game")) {
                    numOfSub = mobileGame.numOfMembers;
                    currentBudget = mobileGame.getBudget();
                    mobileGame.numOfMembers++;
                }
                ProjectManager projectManager = new ProjectManager(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString(),
                        (((JSONObject) i).get("project")).toString(),
                        currentBudget,
                        ((Long) ((JSONObject) i).get("partInProject")).doubleValue(),
                        numOfSub);
                projectManager.calcPayment();
                allMembers.add(projectManager);
            }
            //Создаю и добавляю руководителя направления
            JSONArray senManagerArray = (JSONArray) jsonObject.get("SeniorManager");
            for (Object i : senManagerArray) {
                String stringArr = (((JSONObject) i).get("project")).toString();
                String[] projNames = stringArr.split(",");
                SeniorManager seniorManager = new SeniorManager(
                        ((Long) ((JSONObject) i).get("id")).intValue(),
                        (((JSONObject) i).get("name")).toString());
                seniorManager.projectsNames = (projNames);
                seniorManager.budget = undWaterDrone.getBudget() + mobileGame.getBudget();
                seniorManager.numOfParticiples = undWaterDrone.numOfMembers + mobileGame.numOfMembers;
                seniorManager.calcPayment();
                allMembers.add(seniorManager);
            }

        } catch (FileNotFoundException ex) {
            ex.getMessage();
        } catch (ParseException ex) {
            ex.getMessage();
        } catch (IOException ex) {
            ex.getMessage();
        }

//Вывожу данные в виде таблицы

//        String[][] empl = new String[allMembers.size()+1][3];
//        empl[0][0] = "Cотрудник    ";
//        empl[0][1] = "Должность    ";
//        empl[0][2] = "Зарплата     ";
//        System.out.println(Arrays.toString(empl[0]));
//        for (int i = 1; i < allMembers.size()+1; i++) {
//            empl[i][0] = allMembers.get(i-1).getName();
//            empl[i][1] = allMembers.get(i-1).position;
//            empl[i][2] = Double.toString(allMembers.get(i-1).payment);
//            System.out.println(Arrays.toString(empl[i]));
//        }



        ArrayList<String> name = new ArrayList<>();
        ArrayList<String> position = new ArrayList<>();
        ArrayList<Double> salary = new ArrayList<>();

        for (Employee i : allMembers) {
            name.add(i.getName());
            position.add(i.position);
            salary.add(i.getPayment());
        }
        System.out.println("Сотрудник    "+"Должность   "+"Зарплата   ");
        for (int i = 0; i < name.size(); i++) {
            System.out.println(" "+name.get(i)+" "+position.get(i)+" "+salary.get(i)+" ");
        }

    }


}
