/**
 * Расчет оплаты исходя из отработанного времени (часы умножаются на ставку).
 */
public interface WorkTime {
    int getPaymentForWorkTime();
}
